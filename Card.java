package masdari.mikaiil.app;

/**
 * Created by mikaiilmasdari on 5/13/17.
 */
abstract public class Card {
    private String name;
    private String description;

    public Card(String name,String description){
        setDescription(description);
        setName(name);
    }

    @Override
    public String toString() {
        return this.name+" "+this.description;
    }

    public boolean equals(Card c){
        if(this.name.equals(c.name)&& this.description.equals(c.description)){
            return true;
        }
        return false;
    }
    public void setName(String name){
        this.name=name;
    }
    public void setDescription(String description){
        this.description=description;
    }
    public String getName(){
        return name;
    }
    public String getDescription(){
        return description;
    }
}
